/**
 * Plain do-nothing code but create a GSM instance
 * On reset current raises to ~40-50 mA and stabilizes after a couple of seconds at 21.5 +/- 0.1
 */

#include <MKRGSM.h>

GSM gsmAccess;

void setup() {
}

void loop() {
}
