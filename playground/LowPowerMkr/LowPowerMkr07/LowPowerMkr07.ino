/**
 * Plain do-nothing code with a call to LowPower.deepSleep(10000) and the built-in LED being lit
 *
 * On reset current raises to ~34 mA and stabilizes after a couple of seconds at
 *  * 16.1 mA +/- 0.1 Then raises to 24.8 +/- 0.1 (at ~12 secs) (10 samples) (note: a couple of samples around 15 mA)
 *
 * Turning of the LED now gives
 * 11.5 +/- 0.1 mA and 19.0 +/- 0.1 mA after 12 secs which is inconsistent with yesterday's measure of ~15.1 mA (example 06)
 */

#include <ArduinoLowPower.h>
void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    LowPower.deepSleep(10000);
}

void loop() {
}
