#include <Adafruit_SSD1306.h>

#define OLED_ADDR   0x3C

Adafruit_SSD1306 display(128, 64);

void setup() {
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(17,27);
  display.print("EFD OLED");
  display.display();
}

void loop() {}

