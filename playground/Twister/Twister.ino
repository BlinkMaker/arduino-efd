/*
 * A dancing robot
 */

#include <olimexcontroller.h>

long int timer;
int choreographyPosition = 0;
const int motorPower = 255;
OlimexController olimexController(3, 4, 5, 6, 7, 8);

void setup() {}

void loop() {
    const char choreography[] = "xxxaaaaddaaddaaddwwsswwssadadadaxxx";
    if(millis() - timer > 300) {
        timer = millis();
        choreographyPosition++;
        switch (choreography[choreographyPosition % strlen(choreography)]) {
            case 'a':   olimexController.setPower(0, motorPower);
                        olimexController.setPower(1, -motorPower);
                        break;
            case 'd':   olimexController.setPower(0, -motorPower);
                        olimexController.setPower(1, motorPower);
                        break;
            case 'w':   olimexController.setPower(0, motorPower);
                        olimexController.setPower(1, motorPower);
                        break;
            case 's':   olimexController.setPower(0, -motorPower);
                        olimexController.setPower(1, -motorPower);
                        break;
            default:    olimexController.setPower(0, 0);
                        olimexController.setPower(1, 0);
                        break;
        }
    }
}
