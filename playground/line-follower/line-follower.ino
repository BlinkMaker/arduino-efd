
#include "olimexcontroller.h"
#define left A0
#define right A1

void setup() {
  pinMode(left,INPUT);
  pinMode(right,INPUT);
}

void loop(){
  OlimexController motorController(4, 5, 6, 7); 
  motorController.setup();

  if(digitalRead(left)==0 && digitalRead(right)==0){
    motorController.execute('w');
    delay(100);
  }
  else if(digitalRead(left)==0 && !analogRead(right)==0){
    motorController.execute('a');
    delay(400);
  }
  else if(!digitalRead(left)==0 && digitalRead(right)==0){
    motorController.execute('d');
    delay(400);
  }
  else if(!digitalRead(left)==0 && !digitalRead(right)==0){
    //stop if line is too wide
    motorController.execute('x');
  }
}
