/*
 * Showing the EM signal on an OLED screen (SSD1306), adapting to the signal level.
 *
 * Note: Avoid using A4 and A5 for the antenna as they are used for
 * the screen's I2C bus
 *
 */

#include <Adafruit_SSD1306.h>

#define OLED_ADDR 0x3C

Adafruit_SSD1306 display(128, 64);
uint8_t rowNumber = 0;
int readings[128];
char textBuf[4];
void setup() {
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
}

void loop() {
    long int cumSum = 0;
    for (int j = 0; j < 128; ++j) {
        readings[j] = analogRead(A0);
        cumSum += readings[j];
    }
    const int average = min(cumSum / 128, 999); //cap value of average to fit within three digits
    for (int j = 0; j < 128; ++j) {
        display.drawPixel(j, rowNumber,  readings[j] > average? WHITE : BLACK);
    }
    sprintf(textBuf, "%3d", average);
    display.setTextSize(2);
    display.setTextColor(WHITE, BLACK);
    display.setCursor(0,0);
    display.print(textBuf);
    display.display();
    rowNumber++;
    if (rowNumber == 64) {
        rowNumber = 0;
    }
    delayMicroseconds(2300); //a value of 2300 us on the Arduino Uno makes the 50Hz signal from the network appear as stripes
}
