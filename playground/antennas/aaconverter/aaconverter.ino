/*
 * A program that will transfer the signal on one analog pin and
 * output it on another one in order to drive a device such as
 * a LED or a loudspeaker.
 * A potentiometer in serial can adjust the threshold for activation the loudspeaker and LED
 * 
 * Open serial plotter og monitor to read values of the signal.
 *
 */
int val;
void setup() {
  Serial.begin(9600); 
}
void loop() {
    val = analogRead(A0);
    Serial.println(val);
    analogWrite(A1,val/4);
}
