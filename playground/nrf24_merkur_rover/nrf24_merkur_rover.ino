/*
 *  symax_rover.ino -- An arduino sketch to test the symax protocol
 */

#include <SPI.h>
#include <symax_protocol.h>

//#DEFINE NDEBUG

nrf24l01p wireless;
symaxProtocol protocol;
const int motor1Pin1 = 3;
const int motor1Pin2 = 4;
const int motor2Pin1 = 5;
const int motor2Pin2 = 6;
const int cePin = 8;
const int csPin = 7;
unsigned long time = 0;

void setup() {
    Serial.begin(9600);
    // SS pin must be set as output to set SPI to master !
    pinMode(SS, OUTPUT); //delete?
    wireless.setPins(cePin, csPin);
    // Set power (PWRLOW,PWRMEDIUM,PWRHIGH,PWRMAX)
    wireless.setPwr(PWRLOW);
    protocol.init(&wireless);
    time = micros();
    Serial.println("Start");
    //Motor pins
    pinMode(motor1Pin1, OUTPUT);
    pinMode(motor1Pin2, OUTPUT);
    pinMode(motor2Pin1, OUTPUT);
    pinMode(motor2Pin2, OUTPUT);
}

/*
 * Function reading pitch and roll (right stick left/right)
 * to control the rover wheels
 */
void activateMotors(const int pitch, const int roll) {
if(pitch > 100) { //forward
  digitalWrite(motor1Pin1, 0); //
  digitalWrite(motor1Pin2, 1); //
  digitalWrite(motor2Pin1, 0); //
  digitalWrite(motor2Pin2, 1); //
} else if (pitch < -100) { //backwards
  digitalWrite(motor1Pin1, 1); //
  digitalWrite(motor1Pin2, 0); //
  digitalWrite(motor2Pin1, 1); //
  digitalWrite(motor2Pin2, 0); //
} else if (roll > 100) { //left
  digitalWrite(motor1Pin1, 0); //
  digitalWrite(motor1Pin2, 1); //
  digitalWrite(motor2Pin1, 1); //
  digitalWrite(motor2Pin2, 0); //
} else if (roll < -100) { //right
  digitalWrite(motor1Pin1, 1); //
  digitalWrite(motor1Pin2, 0); //
  digitalWrite(motor2Pin1, 0); //
  digitalWrite(motor2Pin2, 1); //
} else {
  digitalWrite(motor1Pin1, 0); //
  digitalWrite(motor1Pin2, 0); //
  digitalWrite(motor2Pin1, 0); //
  digitalWrite(motor2Pin2, 0); //
}
}

void testMotors() {
/*    digitalWrite(directionLeftPin1, LOW);
    digitalWrite(directionLeftPin2, HIGH);
    digitalWrite(directionRightPin1, LOW);
    digitalWrite(directionRightPin2, HIGH);
    analogWrite(powerLeftPin,0);
    analogWrite(powerRightPin,150);
    */
}

rx_values_t rxValues;
unsigned long newTime;

void loop() {
    time = micros();
    uint8_t value = protocol.run(&rxValues);
    newTime = micros();
    switch(value) {
    case  NOT_BOUND:
        Serial.println("Not bound");
        break;
    case  BIND_IN_PROGRESS:
        Serial.println("Bind in progress");
        break;
    case BOUND_NEW_VALUES:
#ifndef NDEBUG
        Serial.print(newTime - time);
        Serial.print(" :\t");Serial.print(rxValues.throttle);
        Serial.print("\t"); Serial.print(rxValues.yaw);
        Serial.print("\t"); Serial.print(rxValues.pitch);
        Serial.print("\t"); Serial.print(rxValues.roll);
        Serial.print("\t"); Serial.print(rxValues.trim_yaw);
        Serial.print("\t"); Serial.print(rxValues.trim_pitch);
        Serial.print("\t"); Serial.print(rxValues.trim_roll);
        Serial.print("\t"); Serial.print(rxValues.video);
        Serial.print("\t"); Serial.print(rxValues.picture);
        Serial.print("\t"); Serial.print(rxValues.highspeed);
        Serial.print("\t"); Serial.println(rxValues.flip);
#endif
         activateMotors(rxValues.pitch, rxValues.roll);
        break;
    case BOUND_NO_VALUES:
        //Serial.print(newTime - time); Serial.println(" : ----");
        break;
    default:
        break;
    }
}
