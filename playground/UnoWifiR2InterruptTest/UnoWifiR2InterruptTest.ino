/*
 * Testing the timer interrupt of the Uno Wifi Rev. 2 board.
 * Modify the TCB1.CCMP value to get a different interrupt frequency
 */

int count = 0;

void setup() {
  Serial.begin(115200);
  TCB1.CTRLB = TCB_CNTMODE_INT_gc; // Use timer compare mode
  TCB1.CCMP = 25000; // Value to compare with. This is 1/10th of the tick rate (250000), so 10 Hz
  TCB1.INTCTRL = TCB_CAPT_bm; // Enable the interrupt
  TCB1.CTRLA = TCB_CLKSEL_CLKTCA_gc | TCB_ENABLE_bm; // Use Timer A as clock, enable timer
}

ISR(TCB1_INT_vect) {
  count++;
  TCB1.INTFLAGS = TCB_CAPT_bm;
}

void loop() {
  Serial.println(count);
}
