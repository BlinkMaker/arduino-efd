/**
 * A simple program to show how to send data to Arduino
 * through the serial interface
 *
 * Usage: upload the program to the Arduino, open the serial
 * monitor, type something in the serial monitor's top line
 * and press send.
 */


int incomingByte;

void setup() {
        Serial.begin(9600);
}

void loop() {
        int availableBytes = Serial.available();
        if ( availableBytes> 0) {
                Serial.print("Available: ");
                Serial.print(availableBytes);
                incomingByte = Serial.read();
                Serial.print(", I received: ");
                Serial.println(incomingByte, DEC);
        }
}
