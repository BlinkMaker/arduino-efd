int g_led =7;
int r_led =8;
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>

#define OLED_ADDR   0x3C

Adafruit_SSD1306 display(-1);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

/*
Project: ADXL335 (GY-61) three-axis accelerometer module 
Function: Open the serial monitor window, where you can see the data from ADXL335
displayed. Note: VCC connected to 5V and ADXL335 Vs is 3.3 V
*/
//**********************************************************************
const int xPin = A1;//x-axis of the accelerometer
const int yPin = A2;//y-axis of the accelerometer
const int zPin = A3;//z-axis of the accelerometer (only on 3-axis models)

int cal_x=0;
int cal_y=0;
int cal_z=0;
//**********************************************************************
void setup(){
 Serial.begin(9600); //initialize serial communication at 9600 bps

 // initialize and clear display
 pinMode(g_led, OUTPUT);
  pinMode(r_led, OUTPUT);

cal_x = analogRead(xPin);  //read from xpin
 delay(1); //sets 1ms delay
 cal_y = analogRead(yPin);  //read from ypin
 delay(1); //sets 1 ms delay
  cal_z = analogRead(zPin);  //read from zpin


 
 
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();
  display.display();

  // display a pixel in each corner of the screen
  display.drawPixel(0, 0, WHITE);
  display.drawPixel(127, 0, WHITE);
  display.drawPixel(0, 63, WHITE);
  display.drawPixel(127, 63, WHITE);

  // display a line of text
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(17,27);
  display.print("EFD OLED");

  // update display with all of the above graphics
  display.display();
  delay(2000);
  display.clearDisplay();
  display.display();
}

void loop(){
  int first=1;

  
 int x = analogRead(xPin);  //read from xpin
 delay(1); //sets 1ms delay
 int y = analogRead(yPin);  //read from ypin
 delay(1); //sets 1 ms delay
 int z = analogRead(zPin);  //read from zpin
 
 float new_x = abs(cal_x-x)*100/132;
 float new_y = abs(cal_y-y)*100/132;
 float new_z = abs(cal_z-z)*100/132;
 
 
Serial.print("x: ");
Serial.print(new_x); 
Serial.print("\t");
Serial.print("y: ");
Serial.print(new_y);
Serial.print("\t");
Serial.print("z: ");
Serial.print(new_z);  
Serial.print("\n");

  display.clearDisplay();
  display.display();

  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print("x: ");
  display.println(new_x); 

  display.print("y: ");
  display.println(new_y);

  display.print("z: ");
  display.print(new_z);  

  display.display();
  if(new_x>5 || new_y>5 || new_z>5){
  digitalWrite(r_led,HIGH);
    digitalWrite(g_led,LOW);
  
  }
  else{
    digitalWrite(r_led,LOW);
    digitalWrite(g_led,HIGH);  
  }

/*float zero_G = 338.0; //ADXL335 power supply by Vs 3.3V, so:(3.3 V / 5 V )*1024 = 676 / 2 = 338
float zero_Gx=331.5;//the zero_G output of x axis:(x_max + x_min)/2
float zero_Gy=329.5;//the zero_G outgput of y axis:(y_max + y_min)/2
float zero_Gz=340.0;//the zero_G output of z axis:(z_max + z_min)/2

float scale = 67.6;//power supply by Vs 3.3V: (((3.3 V / 5 V) *1024)*330mv/g )/ 3.3 V  = 67.6 g
float scale_x = 65;//the scale of x axis: (x_max/3.3 V) *330 mv/g
float scale_y = 68.5;//the scale of y axis: (y_max/3.3 V)*330 mv/g
float scale_z = 68;//the scale of z axis: (z_max/3.3 V)*330 mv/g

Serial.print(((float)x - zero_Gx)/scale_x);  //print x value on serial monitor
Serial.print("\t");
Serial.print(((float)y - zero_Gy)/scale_y);  //print y value on serial monitor
Serial.print("\t");
Serial.print(((float)z - zero_Gz)/scale_z);  //print z value on serial monitor
Serial.print("\n");
*/
delay(200);
}
