// Include the Servo library 
#include <Servo.h> 
// Declare the Servo pin 
int servoPin = 4; 
// Create a servo object 
Servo Servo1; 


#include <SPI.h>
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>

int sensorValue;


// OLED display TWI address
#define OLED_ADDR   0x3C

Adafruit_SSD1306 display(-1);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

void setup() {
  Serial.begin(9600);
   // We need to attach the servo to the used pin number 
   Servo1.attach(servoPin); 
  // initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();
  display.display();

  // display a pixel in each corner of the screen
  display.drawPixel(0, 0, WHITE);
  display.drawPixel(127, 0, WHITE);
  display.drawPixel(0, 63, WHITE);
  display.drawPixel(127, 63, WHITE);

  // display a line of text
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(17,27);
  display.print("EFD OLED");

  // update display with all of the above graphics
  display.display();
  delay(2000);
  display.clearDisplay();
  display.display();
}

void loop() {
  sensorValue = digitalRead(3);       // read digital input pin 3

  
if(sensorValue==1){
  
  display.clearDisplay();
  display.display();
  
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Activating");
  display.println("Death");
  display.print  ("Machine!!");
  display.display();
  Servo1.write(0); 
  delay(2500); 
  Servo1.write(180); 
  delay(2500);    
  Servo1.write(95); 
  display.clearDisplay();
}
else{

  display.display();

  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Push");
  display.println("The");
  display.print("Button!");
  display.display();


}
  // put your main code here, to run repeatedly:

}

