// Include the Servo library 
#include <Servo.h> 
// Declare the Servo pin 
int servoPin = 3; 
// Create a servo object 
Servo Servo1; 
//OLED
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#define OLED_ADDR   0x3C

int sensorValue;

Adafruit_SSD1306 display(-1);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

//Ultrasonic sensor pins
int inputPin  = 4;    // define pin for sensor echo
int outputPin = 5;    // define pin for sensor trig

void setup() {
    Serial.begin(9600);
       // We need to attach the servo to the used pin number 
   Servo1.attach(servoPin); 
    pinMode(inputPin, INPUT);
    pinMode(outputPin, OUTPUT);
    // initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();
  display.display();

  // display a pixel in each corner of the screen
  display.drawPixel(0, 0, WHITE);
  display.drawPixel(127, 0, WHITE);
  display.drawPixel(0, 63, WHITE);
  display.drawPixel(127, 63, WHITE);

  // display a line of text
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(17,27);
  display.print("EFD OLED");

  // update display with all of the above graphics
  display.display();
  delay(2000);
  display.clearDisplay();
  display.display();
}

/*
 * Read the distance read by the ultrasound sensor
 */

void loop() {
       digitalWrite(outputPin, LOW);   // ultrasonic sensor transmit low level signal 2μs
    delayMicroseconds(2);
    digitalWrite(outputPin, HIGH);  // ultrasonic sensor transmit high level signal10μs, at least 10μs
    delayMicroseconds(10);
    digitalWrite(outputPin, LOW);    // keep transmitting low level signal
    float pingTime = pulseIn(inputPin, HIGH);  // read the time in between
    float distance = pingTime*0.034/2;
    Serial.println(distance);
    
  if(distance<=100 && distance>5){  
  display.clearDisplay();
  display.display();

  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print(distance);
  display.print(" CM");
  display.display();
        display.clearDisplay();
  }
  if(distance<=5){  
  display.clearDisplay();
  display.display();

  display.setTextSize(4);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Hello");
  display.print(" :)");
  display.display();
     // Make servo go to 0 degrees 
   Servo1.write(30); 
   delay(500); 
   Servo1.write(150); 
   delay(500);    
      Servo1.write(30); 
   delay(500); 
   Servo1.write(130); 
   delay(500);
                display.clearDisplay();
  }
    if(distance>100){  

  display.display();

  display.setTextSize(3);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Come");
    display.print("Closer");
  display.display();
  }
    delay(250);
}

