Miscellaneous Arduino-based projects
==================================
used at The Danish-French School of Copenhagen to teach children (in particular) about programming and computational thinking.
(L'école franco-danoise http://www.ecolefrancodanoise.dk)

Subdirectories:
 * playground: contains various prototypes with no other purpose than being a short-lived experiment
 * darkside: code for The Dark Side Challenge, during which a semi-autonomous vehicle is used for a virtual visit in another school (or other organizations)
