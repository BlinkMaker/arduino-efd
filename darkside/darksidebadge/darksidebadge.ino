#include <Adafruit_SSD1306.h>

#define OLED_ADDR   0x3C

/*
 * Random'ish pattern generated on an Oled screen
 * to back-light a badge.
 */

Adafruit_SSD1306 display(128, 64);
int intensity = 0;
int intensityChangeDirection = 1;

void setup() {
   display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
   display.clearDisplay();
   display.display();
   Serial.begin(9600);
}

void loop() {
    int seed = millis() % 256;
    for (float i = 0; i< 64; ++i) {
        for (float j = 0; j< 128;++j) {
            display.drawPixel(j, i, (int)floor(i/j*seed) % 10 > intensity? BLACK : WHITE);
        }
    }
    intensity += intensityChangeDirection;
    if (intensity == 10 || intensity == 0) {
        intensityChangeDirection = -intensityChangeDirection;
    }
    Serial.println(intensity);
    display.display();
}
