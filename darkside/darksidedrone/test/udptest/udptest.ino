/*
 * A small program to test the UDP connectivity between the Arduino board and the server
 * To debug over wifi, run (on a receiving server on the same subnet):
 *   nc -l -u -p 9999
 *
 * Remote control the robot over WiFi / UDP with, for now, the commands 't' (arm and start) and 'x' (disarm/stop):
 *   stty raw && nc -u 192.168.43.14 3000
 *
 * Note: use 'netstat | grep 3000' on the server to get the drone's IP
 */

#include <WiFiNINA.h>
#include <WiFiUdp.h>
const char ssid[] = "Jolly 1";
const char password[] = "aaaaaaaa";
const IPAddress serverIp(192,168,43,112);
const int serverPortNumber = 9999;
WiFiUDP udp;
long int iterationCounter = 0;

void setup() {
    Serial.begin(115200);
    WiFi.begin(ssid, password);
    udp.begin(3000);
}

void loop() {
    iterationCounter++; //millis() doesn't seem to work with the WiFi
    if(udp.parsePacket() > 0) {
        char udpReceiveBuffer[20];
        udp.read(udpReceiveBuffer, 20);
        Serial.println(udpReceiveBuffer);
        char debugString[70];
        sprintf(debugString, "Received: %s\n", udpReceiveBuffer);
        udp.beginPacket(serverIp, serverPortNumber);
        udp.write(debugString, strlen(debugString));
        udp.endPacket();
    } else if (iterationCounter % 1000 == 0) {
        const char pingString[] = "Ping server.\n";
        udp.beginPacket(serverIp, serverPortNumber);
        udp.write(pingString, strlen(pingString));
        udp.endPacket();
    }
}
