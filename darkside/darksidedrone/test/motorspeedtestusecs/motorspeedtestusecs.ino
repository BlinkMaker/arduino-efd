/*
 * Testing the writeMicroseconds pulse width function
 *
 * Remove propellers before running this test.
 *
 */

#include <Servo.h>



Servo motors[4];
//int motorPins[] = {5,6,10,11};
int motorPins[] = {5,6,9,10}; //note to self: orange, black, blue, white

int idlePulseWidth = 1000; //1500 //may vary depending on ESC
int maxPulseExtraWidth = 300; //150

void setup() {
    Serial.begin(115200);
    for (int i = 0; i < 4; ++i) {
        motors[i].attach(motorPins[i]);
        motors[i].writeMicroseconds(idlePulseWidth);
    }
    delay(3000);
    for (int nbRuns = 0; nbRuns < 3; ++nbRuns) {
        for (int speed = idlePulseWidth; speed <= idlePulseWidth + maxPulseExtraWidth; speed += 50)  {
          Serial.print("Speed: ");
          Serial.println(speed);
            for (int i = 0; i < 4; ++i) {
                motors[i].writeMicroseconds(speed);
                delay(250);
                motors[i].writeMicroseconds(idlePulseWidth);
            }
        }
        for (int i = 0; i < 4; ++i) {
            motors[i].writeMicroseconds(1200);
        }
        delay(1500);
        for (int i = 0; i < 4; ++i) {
            motors[i].writeMicroseconds(idlePulseWidth);
        }
    }
}

void loop() {}
