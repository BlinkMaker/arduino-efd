//using the TinyGPS library from http://arduiniana.org/libraries/tinygps/
#include "TinyGPS.h"
#include "wificonfig.h"
#include "HttpStreamClient.h"
#include "StringStream.h"
#include <MKRGSM.h>
#include <ArduinoLowPower.h>

/*
 * FreeTracker - a free/libre GPS tracking unit
 *
 * Gets a GPS fix and sends its position, then sends a shutdown signal
 * to timer who turns it off.
 *
 * If the GPS fix isn't obtained before the defined time out, the unit sends
 * and error report.
 *
 * Built for an MKR GSM 1400
 */

TinyGPS gps;
GSMClient tcpClient;
GPRS gprs;
GSM gsmAccess;
HttpStreamClient httpClient(&tcpClient);
long int timeOut = 15*60*1000; //shut down after 15 minutes if no fix was found.
uint32_t sleepPeriod = 15*60*1000; //sleep for 15 minutes
bool connectionOK = false;
const int gpsEnablePin = 10; //Note: active LOW

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    Serial1.begin(9600);
}

void deactivatePeripherals() {
    pinMode(gpsEnablePin, INPUT); //set the pin to floating, pull-up resistor will pull it to HIGH (inactive)
    digitalWrite(LED_BUILTIN, LOW);
    LowPower.deepSleep(sleepPeriod);
}

void loop() {
    digitalWrite(LED_BUILTIN, connectionOK);
    pinMode(gpsEnablePin, OUTPUT);
    digitalWrite(gpsEnablePin, LOW);
    if (!connectionOK) {
        connectionOK = (gsmAccess.begin(PINNUMBER) == GSM_READY) &&
                       (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY);
    }
    if (Serial1.available() && connectionOK) {
        int c = Serial1.read();
        if (gps.encode(c)) {
            long lat, lon;
            unsigned long fix_age, time, date;
            gps.get_position(&lat, &lon, &fix_age);
            gps.get_datetime(&date, &time, &fix_age);
            char measurementRequest[70];
            char measurementContent[30];
            sprintf_P(measurementContent, PSTR("%d,%d,%d"),lat, lon,time);
            sprintf_P(measurementRequest, PSTR("/write/measurement/%s/%s"), robotId, measurementContent);
            httpClient.get(commandServer, measurementRequest, commandServerPort);
            deactivatePeripherals();
        }
    }
    if (millis() > timeOut) {
            deactivatePeripherals();
    }
}
