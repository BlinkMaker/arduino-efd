#include "wificonfig.h"
#include "SoftwareSerial.h"
#include "HttpStreamClient.h"
#include "Sim7000Client.h"

/*
 * A test covering the full startup process until first message sent
 * Typical result:
 * GSM fix time: 37
 * GPS (total) time: 66
 */

SoftwareSerial* ss = new SoftwareSerial(8,9);
Sim7000Client sim7000Client(ss);
HttpStreamClient httpClient(&sim7000Client);
bool connectionOK = false;
const int activateMainCircuitPin = 4; //Note: active LOW
bool testSucceeded = false;
long int gsmTime;
long int gpsFixTime;

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    ss->begin(9600);
    Serial.begin(9600);
    pinMode(activateMainCircuitPin, OUTPUT);
    digitalWrite(activateMainCircuitPin, LOW); //activate
}

void loop() {
    if (millis() > 1000*69*15 || testSucceeded) {
        digitalWrite(activateMainCircuitPin, HIGH); //deactivat'ish
        pinMode(activateMainCircuitPin, INPUT); //set pin to floating, pull-up will turn off
        const int halfPeriod = testSucceeded? 1000 : 200;
        if ((millis() / halfPeriod) % 2 == 0) {
            digitalWrite(LED_BUILTIN, HIGH);
        } else {
            digitalWrite(LED_BUILTIN, LOW);
        }
    } else {
        LatLonTime latLonTime;
        if (!connectionOK) {
            Serial.println("attempting connection");
            connectionOK = sim7000Client.begin(PINNUMBER);
            gsmTime = millis() / 1000;
        }
        if (connectionOK) {
            Serial.println("getting gps position");
            Serial.println("latLonTime.time (length and content)");
            Serial.println(strlen(latLonTime.time));
            Serial.println(latLonTime.time);
            latLonTime = sim7000Client.gpsLatLonTime();
            if (connectionOK && strlen(latLonTime.time)==14) {
                gpsFixTime = millis() / 1000;
                Serial.println("about to send sample");
                char measurementRequest[90];
                char measurementContent[50];
                sprintf_P(measurementContent, PSTR("%ld,%ld,%s"), latLonTime.lat, latLonTime.lon, latLonTime.time);
                sprintf_P(measurementRequest, PSTR("/write/measurement/%s/%s"), robotId, measurementContent);
                Serial.println(measurementRequest);
                const char* response = httpClient.get(commandServer, measurementRequest, commandServerPort);
                if (response != 0x0) {
                    Serial.println("Test succeeded, got response:");
                    Serial.println(response);
                    testSucceeded = true;
                    Serial.print("GSM fix time: ");
                    Serial.println(gsmTime);
                    Serial.print("GPS time: ");
                    Serial.println(gpsFixTime);
                    Serial.print("Total time: ");
                    Serial.println(millis() / 1000);
                } else {
                    Serial.println("Test failed, invalid response");
                }
            }
        }
    }
}
