package darksidecommander;
import java.io.IOException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/*
 * A class managing the read requests. Each request is specific to a given robotId,
 * resource type (command, measurement, photo, diagnostic).
 *
 * Optionally, a given historical resource can be fetched by indicating its id (an integer).
 * If no id is given, the latest instance is typically fetched. If a negative id -n is given,
 * the n'th latest instance is returned.
 *
 */

@RestController
public class ReadRequestController {

    @Value("${commandpath}")
    private String commandPath;

    @Value("${measurementpath}")
    private String measurementPath;

    @Value("${photopath}")
    private String photoPath;

    @Value("${diagnosticpath}")
    private String diagnosticPath;

    @GetMapping(value = {"/read/command/{robotId}/{requestId}", "/read/command/{robotId}"})
    public String readCommand(@PathVariable String robotId,
                              @PathVariable Optional<Integer> requestId) throws IOException {
        if (requestId.isPresent()) {
            return new String(StorageService.respondToReadRequest(robotId, requestId, commandPath));
        }
        if (RobotStatusService.newCommandIsAvailableFor(robotId) != null &&
            RobotStatusService.newCommandIsAvailableFor(robotId)) {
            RobotStatusService.commandWasServedTo(robotId);
            return new String(StorageService.respondToReadRequest(robotId, requestId, commandPath));
        } else {
            return "";
        }
    }

    @GetMapping(value = {"/read/measurement/{robotId}/{requestId}", "/read/measurement/{robotId}"})
    public String readMeasurement(@PathVariable String robotId,
                                  @PathVariable Optional<Integer> requestId) throws IOException {
        byte[] measurement = StorageService.respondToReadRequest(robotId, requestId, measurementPath);
        return measurement == null? "" : new String(measurement);
    }

    @GetMapping(value = {"/read/photo/{robotId}/{requestId}", "/read/photo/{robotId}"}, produces = MediaType.IMAGE_JPEG_VALUE)    
    public byte[] readPhoto(@PathVariable String robotId,
                                  @PathVariable Optional<Integer> requestId) throws IOException {
        return StorageService.respondToReadRequest(robotId, requestId, photoPath);
    }

    @GetMapping(value = {"/read/diagnostic/{robotId}/{requestId}", "/read/diagnostic/{robotId}"})
    public String readDiagnostic(@PathVariable String robotId,
                                  @PathVariable Optional<Integer> requestId) throws IOException {
        return new String(StorageService.respondToReadRequest(robotId, requestId, diagnosticPath));
    }

}