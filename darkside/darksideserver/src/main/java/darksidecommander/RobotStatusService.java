package darksidecommander;

import java.util.HashMap;

public class RobotStatusService {
    private static HashMap<String, Boolean> newCommandIsAvailableFor = new HashMap<String, Boolean>();

    static public void commandWasServedTo(String robotId) {
        newCommandIsAvailableFor.put(robotId, false);
    }

    static public void commandWasReceivedFor(String robotId) {
        newCommandIsAvailableFor.put(robotId, true);
    }

    static public Boolean newCommandIsAvailableFor(String robotId) {
        return newCommandIsAvailableFor.get(robotId);
    }
}
