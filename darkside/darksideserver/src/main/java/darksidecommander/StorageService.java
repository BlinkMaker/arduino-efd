package darksidecommander;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

/*
 * A class managing storage and retrieval of the incoming resp. outgoing data.
 */

public class StorageService {

    static public String writeRequestToStorage(String robotId, byte[] bs, String directoryPath) throws IOException {
        final Integer lastFileName = StorageService.getLastFilename(directoryPath, robotId);
        final String newFileName = lastFileName == null? "0" : "" + (lastFileName + 1);
        Files.createDirectories(Paths.get(directoryPath + "/" + robotId));
        Files.write(Paths.get(directoryPath + "/" + robotId + "/" + newFileName), bs);
        return newFileName;
    }

    static byte[] respondToReadRequest(String robotId, Optional<Integer> requestId,
            String directoryPath) throws IOException {
        Integer fileName;
        if (!requestId.isPresent()) { // get latest entry
            fileName = StorageService.getLastFilename(directoryPath, robotId);
            if (fileName == null) {
                return ("Error: " + directoryPath + " not available for " + robotId).getBytes();
            }
        } else {
            if(requestId.get() >= 0) {
                fileName = requestId.get();
            } else {
                Integer lastFileName = StorageService.getLastFilename(directoryPath, robotId); 
                fileName = lastFileName + requestId.get();
            }
        }
        try {
            final byte[] rawContent = Files.readAllBytes(Paths.get(directoryPath + "/" + robotId + "/" + fileName));
            return rawContent;
        } catch (NoSuchFileException e) {
            return ("Error: " + directoryPath + " " + requestId + " not available for " + robotId).getBytes();
        }
    }

    static private Integer getLastFilename(String directoryPath, String robotId) {
        final String[] fileList = new File(directoryPath + "/" + robotId).list();
        if (fileList != null && fileList.length > 0) {
            return  Integer.parseInt(Arrays.asList(fileList).parallelStream()
                    .max(Comparator.comparing(s -> Integer.parseInt(s))).get());
        } else {
            return null;
        }
    }
}
