#ifndef ARDUCAMSTREAM_H
#define ARDUCAMSTREAM_H

#include <ArduCAM.h>
#include <Wire.h>
#include <SPI.h>

class ArduCamStream : public Stream {
public:
    bool begin(int _SPI_CS);
    bool capture(void);
    int available(void);
    int read(void);
    int peek(void) { return 0; }; //not implemented
    size_t write(uint8_t) { return 0; }; //not applicable
    void flush(void) {}; // not applicable
    int fifoSize(void);
    void SPIdisable(void);
    bool terminatorReached(void);
    bool isHeader(void);

private:
    uint8_t curValue = 0, prevValue = 0;
    uint32_t length = 0;
    uint32_t curPos = 0;
    bool is_header = false;
    int SPI_CS;
    ArduCAM myCAM;
};

bool ArduCamStream::begin(int _SPI_CS) {
    SPI_CS = _SPI_CS;
    pinMode(SPI_CS,OUTPUT);
    digitalWrite(SPI_CS, HIGH);
    SPI.begin();
#if defined (OV2640_MINI_2MP)
    myCAM = ArduCAM(OV2640, SPI_CS );
#elif defined (OV3640_MINI_3MP)
    myCAM = ArduCAM(OV3640, SPI_CS );
#else
    myCAM = ArduCAM(OV5642, SPI_CS );
#endif
    bool cameraFound = false;
    uint8_t vid, pid;
    Wire.begin();
    myCAM.write_reg(0x07, 0x80);
    delay(100);
    myCAM.write_reg(0x07, 0x00);
    delay(100);
    myCAM.write_reg(ARDUCHIP_TEST1, 0x55);
    if (myCAM.read_reg(ARDUCHIP_TEST1) == 0x55){
        cameraFound = true;
    }
    if (!cameraFound) {
        return false;
    }
    #if defined (OV2640_MINI_2MP)
    for (int i = 0; i < 10; ++i) {
        //Check if the camera module type is OV2640
        myCAM.wrSensorReg8_8(0xff, 0x01);
        myCAM.rdSensorReg8_8(OV2640_CHIPID_HIGH, &vid);
        myCAM.rdSensorReg8_8(OV2640_CHIPID_LOW, &pid);
        if ((vid != 0x26 ) && (( pid != 0x41 ) || ( pid != 0x42 ))){
            delay(1000);
        }
        else{
            cameraFound = true;
            break;
        }
    }
    #elif defined (OV3640_MINI_3MP)
    for (int i = 0; i < 10; ++i) {
        //Check if the camera module type is OV3640
        myCAM.rdSensorReg16_8(OV3640_CHIPID_HIGH, &vid);
        myCAM.rdSensorReg16_8(OV3640_CHIPID_LOW, &pid);
        if ((vid != 0x36) || (pid != 0x4C)){
            delay(1000);continue;
        }else{
            cameraFound = true;
            break;
        }
    }
    #else
    for (int i = 0; i < 10; ++i) {
        //Check if the camera module type is OV5642
        myCAM.wrSensorReg16_8(0xff, 0x01);
        myCAM.rdSensorReg16_8(OV5642_CHIPID_HIGH, &vid);
        myCAM.rdSensorReg16_8(OV5642_CHIPID_LOW, &pid);
        if((vid != 0x56) || (pid != 0x42)){
            delay(1000);continue;
        }
        else{
            cameraFound = true;
            break;
        }
    }
    #endif
    if (!cameraFound) {
        return false;
    }
    myCAM.set_format(JPEG);
    myCAM.InitCAM();
    #if defined (OV2640_MINI_2MP)
    myCAM.OV2640_set_JPEG_size(OV2640_320x240);
    #elif defined (OV3640_MINI_3MP)
    myCAM.OV3640_set_JPEG_size(OV3640_320x240);
    #else
    myCAM.write_reg(ARDUCHIP_TIM, VSYNC_LEVEL_MASK);   //VSYNC is active HIGH
    myCAM.OV5642_set_JPEG_size(OV5642_320x240);
    #endif
    return true;
}

int ArduCamStream::read(void) {
    if (available()) {
        prevValue = curValue;
        curValue= myCAM.read_fifo();
        curPos++;
        return curValue;
    } else {
        return 0;
    }
}
int ArduCamStream::available(void) {
    return curPos < length;
}

bool ArduCamStream::terminatorReached(void) {
    return curValue == 0xD9 && prevValue == 0xFF;
}

bool ArduCamStream::isHeader(void) {
    return curValue == 0xD8 && prevValue == 0xFF;
}

bool ArduCamStream::capture(void){
    curPos = 0;
    myCAM.flush_fifo();
    myCAM.clear_fifo_flag();
    myCAM.start_capture();
    delay(1000); //reduce this?
    if(!myCAM.get_bit(ARDUCHIP_TRIG , CAP_DONE_MASK)) {
        return false;
    };
    length = myCAM.read_fifo_length();
    if (length >= MAX_FIFO_SIZE) { //384K
        return false;
    }
    if (length == 0 ) { //0 kb
        return false;
    }
    return true;
}

void ArduCamStream::SPIdisable(void) {
    digitalWrite(SPI_CS, HIGH);
}

int ArduCamStream::fifoSize(void) {
    return length;
}

#endif
