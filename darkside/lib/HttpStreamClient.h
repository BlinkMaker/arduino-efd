#ifndef HTTPSTREAMCLIENT_H
#define HTTPSTREAMCLIENT_H

#include "Client.h"

#ifdef ARDUINO_ARCH_SAMD
    #include <stdarg.h>
    #define sprintf_P sprintf
    #define vsprintf_P vsprintf
#endif

#ifndef serialTimeOut
#define serialTimeOut 1000
#endif

#define RESP_BUFF 232
#define TCP_BUF_SIZE 96

/*
 * A class for making HTTP GET and POST (file upload) requests.
 */

class HttpStreamClient {
public:
    HttpStreamClient(Client* _tcpStream);
    const char* get(const char* host, const char* path, int port);
    const char* postFile(const char* host, const char* path, int port, Stream* fileContent, int contentLength);

private:
    Client* tcpStream;
    char httpResponse[RESP_BUFF];
    void storeHttpResponse();
    bool tcp_printf(const char* format, ...);
};

HttpStreamClient::HttpStreamClient(Client* _tcpStream) : tcpStream(_tcpStream) {}

bool HttpStreamClient::tcp_printf(const char* format, ...) {
    char sendBuffer[TCP_BUF_SIZE];
    va_list arglist;
    va_start( arglist, format );
    vsprintf_P(sendBuffer, format, arglist );
    va_end( arglist );
    return tcpStream->write((const uint8_t*)sendBuffer, strlen(sendBuffer));
}

const char* HttpStreamClient::get(const char* host, const char* path, int port) {
    if(!tcpStream->connect(host, port)) return 0;
    tcp_printf(PSTR("GET "));
    tcp_printf(PSTR("%s"), path);
    tcp_printf(PSTR(" HTTP/1.0\r\nHost:"));
    tcp_printf(PSTR("%s"), host);
    bool connectionOk = tcp_printf(PSTR("\r\nConnection: close\r\n\r\n"));
    if (connectionOk) {
        storeHttpResponse();
    }
    const char* httpBody = strstr(httpResponse, "\r\n\r\n");
    return httpBody ? httpBody + 4 : httpResponse + min(strlen(httpResponse), RESP_BUFF);
}

const char* HttpStreamClient::postFile(const char* host, const char* path, int port, Stream* fileContent, int contentLength) {
    if(!tcpStream->connect(host, port)) return 0;
    const char* contentDelimiter = "----dBHkGVB97sEC";
    const int entityBodyWrapping = 2 * strlen(contentDelimiter) + 90 + 21 + 8 ; //includes misc -'ses \r\n's and such
    char sendBuffer[TCP_BUF_SIZE];
    tcp_printf(PSTR("POST "));
    tcp_printf(PSTR("%s"), path);
    tcp_printf(PSTR(" HTTP/1.0\r\nHost: "));
    tcp_printf(PSTR("%s\r\n"), host);
    tcp_printf(PSTR("Content-Length: %d\r\n"), contentLength + entityBodyWrapping);
    tcp_printf(PSTR("Content-Type: multipart/form-data; boundary="));
    tcp_printf(PSTR("%s\r\n\r\n--%s\r\n"), contentDelimiter, contentDelimiter);
    tcp_printf(PSTR("Content-Disposition: form-data; name=\"file\"; filename=\"img.jpg\"\r\n"));
    tcp_printf(PSTR("Content-Type: image/jpg\r\nConnection: close\r\n\r\n"));
    int pos = 0;
    while (fileContent->available()) {
        const char tmp = fileContent->read();
        sendBuffer[pos] = tmp;
        if (pos == TCP_BUF_SIZE - 1) {
            tcpStream->write((const uint8_t*)sendBuffer, TCP_BUF_SIZE);
            pos = 0;
            for (int i = 0; i < TCP_BUF_SIZE; ++i) {
                sendBuffer[i] = 0;
            }
        } else {
            ++pos;
        }
    }
    if (pos > 0 && pos < TCP_BUF_SIZE) {
        tcpStream->write((const uint8_t*)sendBuffer, pos);
    }
    sprintf(sendBuffer, "\r\n--%s--\r\n", contentDelimiter);
    bool connectionOk = tcpStream->write((const uint8_t*)sendBuffer, strlen(sendBuffer));
    if (connectionOk) {
        storeHttpResponse();
    }
    const char* httpBody = strstr(httpResponse, "\r\n\r\n");
    return httpBody ? httpBody + 4 : httpResponse + min(strlen(httpResponse), RESP_BUFF);
}

void HttpStreamClient::storeHttpResponse() {
    for(int i = 0; i < RESP_BUFF; ++i) {
        httpResponse[i] = 0x0;
    }
    long int timer = millis();
    while(!tcpStream->available() && (millis() - timer < serialTimeOut)) {
        //busy wait
    };
    int i = 0;
    while(tcpStream->available() && i < RESP_BUFF) {
        char tmp = tcpStream->read();
        httpResponse[i++] = tmp;
    }
}

#endif
