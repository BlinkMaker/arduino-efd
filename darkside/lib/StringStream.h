#ifndef STRINGSTREAM_H
#define STRINGSTREAM_H

class StringStream : public Stream {
public:
    void setString(const char* _sourceString);
    int available();
    int read();
    int peek();
    size_t write(uint8_t) { return 0; }; //not applicable
    void flush(void) {}; // not applicable
private:
    int position = 0;
    const char* sourceString;
};

void StringStream::setString(const char* _sourceString) {
    sourceString = _sourceString;
    position = 0;
}

int StringStream::available(void) {
    return strlen(sourceString) - position;
}

int StringStream::read() {
    return sourceString[position++];
}

int StringStream::peek() {
    return sourceString[position + 1];
}

#endif /* STRINGSTREAM_H */
