The Dark Side Rover
===================

Wiring
------
```
Arduino Uno <-> ESP (if separate components):
Rx   <-> Tx
Tx   <-> Rx
V3.3 <-> En
V3.3 <-> Vcc
Gnd  <-> Gnd
```

Arduino Uno <-> ESP (if Robotdyn UNO+WiFi R3 ATmega328P+ESP8266):
```
Dip switch 1: On
Dip switch 2: On (Off when flashing)
Dip switch 3: On
Dip switch 4: On
Dip switch 5: Off
Dip switch 6: Off
Dip switch 7: Off
Dip switch 8: Not used
```

```
Arduino Uno <-> Olimex BB-L298:
Vcc   <-> 1
Vcc   <-> 2
Pin 4 <-> 3
Pin 5 <-> 4
Vcc   <-> 5
Pin 6 <-> 6
Pin 7 <-> 7
```

```
Arduino Uno <-> ArduCam
Scl  17 <-> Scl
Sda  16 <-> Sda
Vcc  15 <-> Vcc
Gnd  14 <-> Gnd
Clk  13 <-> SCK
Mosi 12 <-> Miso
Miso 11 <-> Mosi
Pin  10 <-> CS
``` 

```
Arduino Uno <-> Servo
A3  <-> Signal
Vcc <-> Vcc
Gnd <-> Gnd
```

```
Arduino Uno <-> Ultrasound Sensor
Gnd   <-> Gnd
Vcc   <-> Vcc
Pin 8 <-> Trig
Pin 9 <-> Echo
```


Installation
------------

```
cp wificonfig.h.in wificonfig.h
```
Edit wificonfig.h to fit your access point configuration

Flash the code in darksiderover.ino onto your Uno R3. Note: if you are using the Robotdyn UNO+WiFi R3, remember to set dip switch 2 to off when flashing.

In case something doesn't work, consider flashing the tests in test/ onto the Arduino one by one to make sure that they work.

See the README in the darksideserver directory for instructions on how to set up it up.
