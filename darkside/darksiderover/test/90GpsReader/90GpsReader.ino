//using the TinyGPS library from http://arduiniana.org/libraries/tinygps/
#include "TinyGPS.h"
#include "wificonfig.h"
#include "HttpStreamClient.h"

/*
 * A test to read the GPS (Ublox NEO 6M or clone) and send the data to a server
 * The fix might take some to get.
 * At some point in time (after up to ~15 min) the program should start sending
 * the data and the ===processing=== will start appearing on the serial monitor.
 */

TinyGPS gps;

#ifdef ARDUINO_SAMD_MKRGSM1400
    #include <MKRGSM.h>
    GSMClient tcpClient;
    GPRS gprs;
    GSM gsmAccess;
    HttpStreamClient httpClient(&tcpClient);
#endif

long int measurementTimer = 0;


void setup() {
    Serial1.begin(9600);
    #ifdef ARDUINO_SAMD_MKRGSM1400
    bool connectionOK = (gsmAccess.begin(PINNUMBER) == GSM_READY) &&
                    (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY);

    Serial.println(connectionOK? "Connection Ok" : "Connection Failed");
    #endif
    robotId = "GpsTest";
}

void loop() {
    if (Serial1.available()) {
        int c = Serial1.read();
        Serial.print((char)c);
        if (gps.encode(c)) {
            Serial.println("\n==========processing============");
            long lat, lon;
            unsigned long fix_age, time, date, speed, course;
            unsigned long chars;
            unsigned short sentences, failed_checksum;
            gps.get_position(&lat, &lon, &fix_age);
            gps.get_datetime(&date, &time, &fix_age);
            speed = gps.speed();
            course = gps.course();
            Serial.println(lat);
            if (millis() - measurementTimer > 20*1000) {
                measurementTimer = millis();
                char measurementRequest[70];
                char measurementContent[30];
                sprintf_P(measurementContent, PSTR("%d,%d,%d"),lat, lon,time);
                sprintf_P(measurementRequest, PSTR("/write/measurement/%s/%s"), robotId, measurementContent);
                Serial.println("measurementRequest");
                Serial.println(measurementRequest);
                httpClient.get(commandServer, measurementRequest, commandServerPort);
            }
        }
    }
}
