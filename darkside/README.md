Getting started
===============

In darksiderover
```
cp wificonfig.h.in wificonfig.h
```
Then configure edit wificonfig.h to match your values and flash the rover, possibly modifying the code to match your needs.
Also
```
cp -r lib/ ~/Arduino/libraries/darkside
```


In darksideserver
```
mvn spring-boot:run
```
then point your browser at your-server-name:8080/static/index.html

Type your rover's id in the Robot ID field and the commands you want it to execute in the Command field.
Then press the submit button and the rover should react. The current version of the rover accepts sequences of the letters a,s,d,w,m,p


About The Dark Side Challenge
=============================
A DIY Arduino robot project for teaching kids how to combine machine autonomy with human decision points. And programming.

The goal of the challenge is to build a robot that can autonomously navigate through a terrain where radio contact is sporadic or impossible – just as if it were on the dark side of the Moon or somewhere even farther away.

While achieving that goal, the challengers are learning to program and debug embedded devices and sensors, to imagine, reason and persevere, understand how to properly balance machine intelligence with human interaction. The youngest ones are even learning the alphabet – both in capital and small letters!

The system was built to be as simple and accessible as possible and with the purpose of teaching programming and computational thinking. The interaction with the rover is transactional by design, i.e. a full command is sent, fetched and executed before the next is fetched. This is intentionally non-real time.

This repository contains a progression from the simplest Arduino program (Blink), to more advanced topics in C++, Java and Javascript.

Hardware-wise the system consists of an Arduino Uno R3-based rover communicating via WiFi using an ESP8266,

The current version features an ultrasound distance sensor, a camera, a motor controller and two motors.

The rest of the parts can be 3D printed, or old toys, for instance RC cars, can be used as a chassis.

The rover polls its commands from a server which, if it has a public IP, can be accessed from anywhere. Commands are sent to the server via a web page and/or a REST interface.
